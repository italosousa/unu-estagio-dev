<?php 
//
// ─── GET ────────────────────────────────────────────────────────────────
//

    function get_disciplinas($request, $response) {
        global $mysqli;    

        $notas = array();
        $disciplinas = array();
        
        $sql = $mysqli->query("SELECT * FROM disciplinas") or die($mysqli->error);
        while($row = $sql->fetch_assoc()) {
            $notas[] = $row;
        }

        return $response->withJson($notas, 200); 
    }

    function get_disciplina($request, $response) {
        global $mysqli;

        $idDisciplina = $request->getAttribute('id');
        
        $disciplina = array();
        $notas = array();
        $novasNotas = array();
        $alunos = array();  

        // Dados da disciplina
        $sql = $mysqli->query("SELECT * FROM disciplinas WHERE id = '$idDisciplina' ") or die($mysqli->error);
        $row = $sql->fetch_assoc();
        $disciplina["id"] = $row["id"];
        $disciplina["nome"] = $row["nome"];
        // Notas dos alunos
        $sql = $mysqli->query("SELECT nota_1, nota_2, id_aluno, id_matricula FROM notas WHERE id_disciplina = '$idDisciplina' ") or die($mysqli->error);
        while($row = $sql->fetch_assoc()) {
            $notas[] = $row;
        }
        foreach ($notas as $n){
            $n["media"] = (intval($n["nota_1"]) + intval($n["nota_2"])) / 2;
            $novasNotas[] = $n;
        }
        // Nome dos alunos
        foreach ($novasNotas as $aluno){
            $idAluno = $aluno["id_aluno"];
            $sql = $mysqli->query("SELECT id, nome FROM alunos WHERE id = '$idAluno' ") or die($mysqli->error);
            $row = $sql->fetch_assoc();
            $aluno["id_aluno"] = $row;
            $alunos[] = $aluno;
        }

        $disciplina["alunos"] = $alunos;
        
        return $response->withJson($disciplina, 200);
    }

//
// ─── DELETE ─────────────────────────────────────────────────────────────────────
//

    function deletar_disciplina($request, $response) {
        global $mysqli;

        $mensagem = array();

        $id = $request->getAttribute('id');

        // Verifica se existe
        $sql = $mysqli->query("SELECT id FROM disciplinas WHERE id = '$id'") or die($mysqli->error);
        if($sql->num_rows <= 0) {
            $mensagem["status"] = "error";
            $mensagem["texto"] = "Disciplina não encontrada!";
            return $response->withJson($mensagem, 200);
        }

        // Deleta o registro da disciplina e seus derivados
        $sql = "DELETE FROM disciplinas WHERE id = '$id'";
        if(!$mysqli->query($sql)) {
            $mensagem["status"] = "error";
            $mensagem["texto"] = "Ocorreu um erro ao remover a disciplina!";
        } else {
            // Deleta as notas relacionadas a disciplina
            $sql = "DELETE FROM notas WHERE id_disciplina = '$id'";
            if(!$mysqli->query($sql)) {
                $mensagem["status"] = "error";
                $mensagem["texto"] = "Ocorreu um erro ao remover a as notas da disciplina!";
            } else {
                // Remove a disciplina das strings em matrícula
                $sql = $mysqli->query("SELECT id, disciplinas FROM matriculas") or die($mysqli->error);
                while($row = $sql->fetch_assoc()) {
                    $idAlterar = $row["id"];
                    $disciplinasAntes = $row["disciplinas"];
                    $disciplinas = $row["disciplinas"];
                    $disciplinas = explode(";", $disciplinas);
                    // Percorre as disciplinas
                    for($i = 0; $i < count($disciplinas); $i++) {
                        if($disciplinas[$i] == $id) {
                            $disciplinas[$i] = null;
                        }
                    }
                    // Filtra o array
                    $novoArray = array_filter($disciplinas);
                    // Monta a nova string
                    $novaString = implode(";", $novoArray);

                    if($novaString !== $disciplinasAntes) {
                        // Faz update da string
                        $mysqli->query("UPDATE matriculas SET disciplinas='$novaString' WHERE id = '$idAlterar'") or die($mysqli->error);
                    }
                }
                
                $mensagem["status"] = "success";
                $mensagem["texto"] = "Disciplina removida com sucesso!";
            }
        }

        return $response->withJson($mensagem, 200);
    }

// ────────────────────────────────────────────────────────────────────────────────

//
// ─── PUT ────────────────────────────────────────────────────────────────────────
//

function editar_disciplina($request, $response) {
    global $mysqli;
    $error = false;
    // $mensagem = array();
    // $mensagem["status"] = "ok";
    // return $response->withJson($mensagem, 200);
    $mensagem = array();
    $id = $request->getAttribute('id');

    $jsonString = $request->getBody();
    $infos = json_decode($jsonString, true);
    $nome = $infos['nome'];
    
    // Verifica se a disciplina existe
    $sql = $mysqli->query("SELECT id FROM disciplinas WHERE id = '$id'") or die($mysqli->error);
    if($sql->num_rows <= 0) {
        $error = true;
        $mensagem["texto"] = "A disciplina não existe!";
    }
    
    // Altera as infos na db
    if(!$error) {
        $sql = "UPDATE disciplinas SET nome='$nome' WHERE id = '$id'";
        if($mysqli->query($sql)) {
            $mensagem["status"] = "success";
            $mensagem["texto"] = "Disciplina atualizada com sucesso.";
        } else {
            $mensagem["status"] = "error";
            $mensagem["texto"] = "Erro ao atualizar a disciplina.";
        }
    } else {
        $mensagem["status"] = "error";
    }

    return $response->withJson($mensagem, 200);
}

// ────────────────────────────────────────────────────────────────────────────────

//
// ─── POST ───────────────────────────────────────────────────────────────────────
//

function cadastrar_disciplina($request, $response) {
    global $mysqli;

    $mensagem = array();

    $jsonString = $request->getBody();
    $infos = json_decode($jsonString, true);
    $nome = $infos["nome"];

    $sql = "INSERT INTO disciplinas (nome) VALUES ('$nome')";
    if($mysqli->query($sql)) {
        $mensagem["status"] = "success";
        $mensagem["texto"] = "Disciplina cadastrada com sucesso!";
    } else {
        $mensagem["status"] = "error";
        $mensagem["texto"] = "Erro ao cadastrar a disciplina!";
    }

    return $response->withJson($mensagem, 200);
}

?>