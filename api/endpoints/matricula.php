<?php 
//
// ─── GET ────────────────────────────────────────────────────────────────
//

function get_matriculas($request, $response) {
    global $mysqli;

    $dados = array();
    $matriculas = array();
    
    $sql = $mysqli->query("SELECT * FROM matriculas") or die($mysqli->error);
    while($row = $sql->fetch_assoc()) {
        $dados[] = $row;
    }
    
    foreach($dados as $mat) {
        // Dados do aluno
        $idAluno = $mat["id_aluno"];
        $sql = $mysqli->query("SELECT id, nome FROM alunos WHERE id = '$idAluno'") or die($mysqli->error);
        $row = $sql->fetch_assoc();
        $mat["id_aluno"] = $row;
        // Array das disciplinas
        $disciplinas = explode(";", $mat["disciplinas"]);
        $arrayDisciplinas = array();
        $i = 0;
        foreach($disciplinas as $d) {
            $idDisciplina = $disciplinas[$i];
            $sql = $mysqli->query("SELECT id, nome FROM disciplinas WHERE id = '$idDisciplina'") or die($mysqli->error);
            $row = $sql->fetch_assoc();
            $arrayDisciplinas[] = $row;
            $i++;
        }
        $mat["disciplinas"] = $arrayDisciplinas;
    
        $matriculas[] = $mat;
    }
    
    return $response->withJson($matriculas, 200);
}

function get_matricula_detalhes($request, $response) {
    global $mysqli;

    $idMatricula = $request->getAttribute('id');

    $dados = array();
    $matriculas = array();
    
    $sql = $mysqli->query("SELECT * FROM matriculas WHERE id = '$idMatricula'") or die($mysqli->error);
    while($row = $sql->fetch_assoc()) {
        $dados[] = $row;
    }
    
    foreach($dados as $mat) {
        // Dados do aluno
        $idAluno = $mat["id_aluno"];
        $sql = $mysqli->query("SELECT id, nome FROM alunos WHERE id = '$idAluno'") or die($mysqli->error);
        $row = $sql->fetch_assoc();
        $mat["id_aluno"] = $row;
        // Array das disciplinas
        $disciplinas = explode(";", $mat["disciplinas"]);
        $arrayDisciplinas = array();
        $i = 0;
        foreach($disciplinas as $d) {
            $idDisciplina = $disciplinas[$i];
            $sql = $mysqli->query("SELECT id, nome FROM disciplinas WHERE id = '$idDisciplina'") or die($mysqli->error);
            $row = $sql->fetch_assoc();
            $arrayDisciplinas[] = $row;
            // Acrescenta as notas do aluno e a média
            $sqlAluno = $mysqli->query("SELECT * FROM notas WHERE id_aluno = '$idAluno' AND id_disciplina = '$idDisciplina'") or die($mysqli->error);
            while($rowAluno = $sqlAluno->fetch_assoc()) {
                $arrayDisciplinas[$i]["nota1"] = $rowAluno["nota_1"];
                $arrayDisciplinas[$i]["nota2"] = $rowAluno["nota_2"];
                $arrayDisciplinas[$i]["media"] = (intval($rowAluno["nota_1"]) + intval($rowAluno["nota_2"])) / 2;
            }

            $i++;
        }
        $mat["disciplinas"] = $arrayDisciplinas;
    
        $matriculas[] = $mat;
    }

    return $response->withJson($matriculas, 200);
}

// ────────────────────────────────────────────────────────────────────────────────

//
// ─── PUT ────────────────────────────────────────────────────────────────────────
//

function editar_matricula($request, $response) {
    global $mysqli;
    $error = false;

    $mensagem = array();
    $id = $request->getAttribute('id');

    $jsonString = $request->getBody();
    $infos = json_decode($jsonString, true);
    $periodo = $infos['periodo'];
    $disciplinas = $infos['disciplinas'];
    
    // Verifica se a matrícula existe
    $sql = $mysqli->query("SELECT id FROM matriculas WHERE id = '$id'") or die($mysqli->error);
    if($sql->num_rows <= 0) {
        $error = true;
        $mensagem["texto"] = "A matrícula não existe!";
    }
    
    // Altera as infos na db
    if(!$error) {
        $sql = "UPDATE matriculas SET periodo='$periodo', disciplinas='$disciplinas' WHERE id = '$id'";
        if($mysqli->query($sql)) {
            $mensagem["status"] = "success";
            $mensagem["texto"] = "Matrícula atualizada com sucesso.";
        } else {
            $mensagem["status"] = "error";
            $mensagem["texto"] = "Erro ao atualizar a matrícula.";
        }
    } else {
        $mensagem["status"] = "error";
    }

    return $response->withJson($mensagem, 200);
}

// ────────────────────────────────────────────────────────────────────────────────

//
// ─── POST ───────────────────────────────────────────────────────────────────────
//

function cadastrar_matricula($request, $response) {
    global $mysqli;

    $mensagem = array();

    $jsonString = $request->getBody();
    $infos = json_decode($jsonString, true);
    $id_aluno = $infos["id_aluno"];
    $periodo = $infos["periodo"];
    $disciplinas = $infos["disciplinas"];

    // Insere o registro da matrícula
    $sql = "INSERT INTO matriculas (periodo, id_aluno, disciplinas) VALUES ('$periodo', '$id_aluno', '$disciplinas')";
    if(!$mysqli->query($sql)) {
        $mensagem["status"] = "error";
        $mensagem["texto"] = "Erro ao cadastrar a matrícula!";
    } else {
        $idMatricula = $mysqli->insert_id;
        // Insere os registros das disciplinas (notas)
        $disciplinas = explode(";", $disciplinas);
        foreach($disciplinas as $d) {
            $mysqli->query("INSERT INTO notas (nota_1, nota_2, id_aluno, id_disciplina, id_matricula) VALUES ('0', '0', '$id_aluno', '$d', '$idMatricula')") or die($mysqli->error);
        }
        // Mensagem de sucesso
        $mensagem["status"] = "success";
        $mensagem["texto"] = "Matrícula cadastrada com sucesso!";
    }    

    return $response->withJson($mensagem, 200);
}

// ────────────────────────────────────────────────────────────────────────────────

//
// ─── DELETE ─────────────────────────────────────────────────────────────────────
//

function deletar_matricula($request, $response) {
    global $mysqli;

    $mensagem = array();

    $idMatricula = $request->getAttribute('id');

    // Verifica se existe
    $sql = $mysqli->query("SELECT id FROM matriculas WHERE id = '$idMatricula'") or die($mysqli->error);
    $rowsVerifica = $sql->num_rows;
    if($rowsVerifica <= 0) {
        $mensagem["status"] = "error";
        $mensagem["texto"] = "Matrícula não encontrada!";
        return $response->withJson($mensagem, 200);
    }

    // Deleta o registro
    $sql = "DELETE FROM matriculas WHERE id = '$idMatricula'";
    if(!$mysqli->query($sql)) {
        $mensagem["status"] = "error";
        $mensagem["texto"] = "Ocorreu um erro ao deletar a matrícula!";
    } else {
        // Deleta os registros, em 'notas', relacionados a matrícula
        $sql = "DELETE FROM notas WHERE id_matricula = '$idMatricula'";
        if($mysqli->query($sql)) {
            $mensagem["status"] = "success";
            $mensagem["texto"] = "Matrícula removida com sucesso!";
        } else {
            $mensagem["status"] = "error";
            $mensagem["texto"] = "Ocorreu um erro ao deletar a matrícula!";
        }
    }

    return $response->withJson($mensagem, 200);
}

// ────────────────────────────────────────────────────────────────────────────────

?>