<?php
//
// ─── GET ────────────────────────────────────────────────────────────────────────
//

    function get_alunos($request, $response) {
        global $mysqli;

        $dados = array();
        $alunos = array();
        
        // Dados gerais dos alunos
        $sql = $mysqli->query("SELECT * FROM alunos") or die($mysqli->error);
        while($row = $sql->fetch_assoc()) {
            $dados[] = $row;
        }
        
        // Percorre os alunos
        foreach($dados as $aluno) {
            $idAluno = $aluno["id"];
            // Notas
            $notas = array();
            $sql = $mysqli->query("SELECT nota_1, nota_2 FROM notas WHERE id_aluno = '$idAluno'") or die($mysqli->error);
            while($row = $sql->fetch_assoc()) {
                $notas[]["media"] = ($row["nota_1"] + $row["nota_2"]) / 2;
            }
            // Soma todas as notas do aluno
            $todasMedias = 0;
            foreach($notas as $nota) {
                $todasMedias += $nota["media"];
            }
            // Calcula a média geral
            if($notas == null) {
                $mediaGeral = 0;
            } else {
                $mediaGeral = $todasMedias / count($notas);
            }
            $aluno["mediaGeral"] = $mediaGeral;

            $alunos[] = $aluno;
        }

        return $response->withJson($alunos, 200);
    }

    function get_aluno_detalhes($request, $response) {
        global $mysqli;

        $id = $request->getAttribute('id');

        $dados = array();
        $alunos = array();

        $sql = $mysqli->query("SELECT * FROM alunos WHERE id = '$id'") or die($mysqli->error);
        while($row = $sql->fetch_assoc()) {
            $dados[] = $row;
        }

        // Percorre os alunos
        foreach($dados as $aluno) {
            $idAluno = $aluno["id"];
            //
            // ─── NOTAS ──────────────────────────────────────────────────────────────────────
            //
            $notas = array();
            $sql = $mysqli->query("SELECT nota_1, nota_2 FROM notas WHERE id_aluno = '$idAluno'") or die($mysqli->error);
            while($row = $sql->fetch_assoc()) {
                $notas[]["media"] = ($row["nota_1"] + $row["nota_2"]) / 2;
            }
            // Soma todas as notas do aluno
            $todasMedias = 0;
            foreach($notas as $nota) {
                $todasMedias += $nota["media"];
            }
            // Calcula a média geral
            if($notas == null) {
                $mediaGeral = 0;
            } else {
                $mediaGeral = $todasMedias / count($notas);
            }
            $aluno["mediaGeral"] = $mediaGeral;

            //
            // ─── MATRICULAS ─────────────────────────────────────────────────────────────────
            //
            $matriculas = array();
            $sql = $mysqli->query("SELECT id, periodo FROM matriculas WHERE id_aluno = '$idAluno'") or die($mysqli->error);
            while($row = $sql->fetch_assoc()) {
                $matriculas[] = $row;
            }
            $aluno["matriculas"] = $matriculas;

            //
            // ─── DISCIPLINAS ────────────────────────────────────────────────────────────────
            //
            $disciplinas = array();
            $idsMaterias = array();
            // Salva os IDs das disciplinas
            $sql = $mysqli->query("SELECT id_disciplina, nota_1, nota_2 FROM notas WHERE id_aluno = '$idAluno'") or die($mysqli->error);
            while($row = $sql->fetch_assoc()) {
                $idsMaterias[] = $row;
            }
            // Monta o array com os dados das disciplinas
            $i = 0;
            foreach($idsMaterias as $mat) {
                $idMateria = intval($mat["id_disciplina"]);
                $sql = $mysqli->query("SELECT id, nome FROM disciplinas WHERE id = '$idMateria'") or die($mysqli->error);
                while($row = $sql->fetch_assoc()) {
                    $disciplinas[$i] = $row;
                    $disciplinas[$i]["nota1"] = $mat["nota_1"];
                    $disciplinas[$i]["nota2"] = $mat["nota_2"];
                    $disciplinas[$i]["media"] = ($mat["nota_1"] + $mat["nota_2"]) / 2;
                    $i++;
                }
            }
            $aluno["disciplinas"] = $disciplinas;


            $alunos[] = $aluno;
        }
        
        return $response->withJson($alunos, 200);
    }

//
// ─── DELETE ─────────────────────────────────────────────────────────────────────
//

    function deletar_aluno($request, $response) {
        global $mysqli;

        $id = $request->getAttribute('id');

        // Verifica se existe
        $sql = $mysqli->query("SELECT id FROM alunos WHERE id = '$id'") or die($mysqli->error);
        if($sql->num_rows <= 0) {
            $mensagem["status"] = "error";
            $mensagem["texto"] = "Aluno não encontrado!";
            return $response->withJson($mensagem, 200);
        }

        // Deleta o aluno
        $sql = "DELETE FROM alunos WHERE id = '$id'";
        if(!$mysqli->query($sql)) {
            $mensagem["status"] = "error";
            $mensagem["texto"] = "Ocorreu um erro ao remover o aluno!";
        } else {
            // Deleta as matrículas relacionadas ao aluno
            $sql = "DELETE FROM matriculas WHERE id_aluno = '$id'";
            if(!$mysqli->query($sql)) {
                $mensagem["status"] = "error";
                $mensagem["texto"] = "Ocorreu um erro ao remover as matrículas do aluno!";
            } else {
                // Deleta todas as notas relacionadas ao luno
                $sql = "DELETE FROM notas WHERE id_aluno = '$id'";
                if(!$mysqli->query($sql)) {
                    $mensagem["status"] = "error";
                    $mensagem["texto"] = "Ocorreu um erro ao remover as notas do aluno!";
                } else {
                    $mensagem["status"] = "success";
                    $mensagem["texto"] = "Aluno removido com sucesso!";
                }
            }
        }

        return $response->withJson($mensagem, 200);
    }

//
// ─── PUT ────────────────────────────────────────────────────────────────────────
//

function editar_aluno($request, $response) {
    global $mysqli;
    $error = false;
  
    $mensagem = array();
    $id = $request->getAttribute('id');

    $jsonString = $request->getBody();
    $infos = json_decode($jsonString, true);
    $nome = $infos["nome"];
    $data = $infos["data"];
    
    // Verifica se o aluno existe
    $sql = $mysqli->query("SELECT id FROM alunos WHERE id = '$id'") or die($mysqli->error);
    if($sql->num_rows <= 0) {
        $error = true;
        $mensagem["texto"] = "O aluno não existe!";
    }
    
    // Altera as infos na db
    if(!$error) {
        $sql = "UPDATE alunos SET nome='$nome', data='$data' WHERE id = '$id'";
        if($mysqli->query($sql)) {
            $mensagem["status"] = "success";
            $mensagem["texto"] = "Aluno atualizado com sucesso.";
        } else {
            $mensagem["status"] = "error";
            $mensagem["texto"] = "Erro ao atualizar o aluno.";
        }
    } else {
        $mensagem["status"] = "error";
    }

    return $response->withJson($mensagem, 200);
}

// ────────────────────────────────────────────────────────────────────────────────

//
// ─── POST ───────────────────────────────────────────────────────────────────────
//

function cadastrar_aluno($request, $response) {
    global $mysqli;

    $mensagem = array();

    $jsonString = $request->getBody();
    $infos = json_decode($jsonString, true);
    $nome = $infos["nome"];
    $data = $infos["data"];

    $sql = "INSERT INTO alunos (nome, data) VALUES ('$nome', '$data')";
    if($mysqli->query($sql)) {
        $mensagem["status"] = "success";
        $mensagem["texto"] = "Aluno cadastrado com sucesso!";
    } else {
        $mensagem["status"] = "error";
        $mensagem["texto"] = "Erro ao cadastrar o aluno!";
    }

    return $response->withJson($mensagem, 200);
}

// ────────────────────────────────────────────────────────────────────────────────


?>