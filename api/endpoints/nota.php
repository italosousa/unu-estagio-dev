<?php
//
// ─── GET ────────────────────────────────────────────────────────────────────────
//

    function get_nota($request, $response) {
        global $mysqli;

        $idDisciplina = $request->getAttribute('id_disciplina');
        $idAluno = $request->getAttribute('id_aluno');
        $idMatricula = $request->getAttribute('id_matricula');

        $dados = array();

        // Dados das notas
        $sql = $mysqli->query("SELECT id, nota_1, nota_2 FROM notas WHERE id_aluno = '$idAluno' AND id_matricula = '$idMatricula' AND id_disciplina = '$idDisciplina'") or die($mysqli->error);
        while($row = $sql->fetch_assoc()) {
            $dados["id"] = $row["id"];
            $dados["nota_1"] = $row["nota_1"];
            $dados["nota_2"] = $row["nota_2"];
        }  
        // Nome do aluno
        $sql = $mysqli->query("SELECT nome FROM alunos WHERE id = '$idAluno'") or die($mysqli->error);
        while($row = $sql->fetch_assoc()) {
            $dados["aluno"] = $row["nome"];
        }      
        // Nome da disciplina
        $sql = $mysqli->query("SELECT nome FROM disciplinas WHERE id = '$idDisciplina'") or die($mysqli->error);
        while($row = $sql->fetch_assoc()) {
            $dados["disciplina"] = $row["nome"];
        }

        return $response->withJson($dados, 200);
    }

//
// ─── PUT ────────────────────────────────────────────────────────────────────────
//

    function editar_nota($request, $response) {
        global $mysqli;

        $mensagem = array();

        $id = $request->getAttribute('id');

        $jsonString = $request->getBody();
        $infos = json_decode($jsonString, true);
        $notaUm = $infos['nota_1'];
        $notaDois = $infos['nota_2'];

        $sql = "UPDATE notas SET nota_1='$notaUm', nota_2='$notaDois' WHERE id = '$id'";
        if($mysqli->query($sql)) {
            $mensagem["status"] = "success";
            $mensagem["texto"] = "Nota alterada com sucesso!";
        } else {
            $mensagem["status"] = "error";
            $mensagem["texto"] = "Ocorreu um erro ao alterar a nota!";
        }

        return $response->withJson($mensagem, 200);
    }

?>