<?php
	ini_set('magic_quotes_runtime', 0);
    require 'vendor/autoload.php';
    require 'config.php';
	
	$app = new \Slim\App;

    //
    // ─── HEADER ─────────────────────────────────────────────────────────────────────
    //

	$app->options('/{routes:.+}', function ($request, $response, $args) {
   		return $response;
	});

	$app->add(function ($req, $res, $next) {
	    $response = $next($req, $res);
	    return $response
	            ->withHeader('Access-Control-Allow-Origin', '*')
	            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
	            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
    });
    
    // ────────────────────────────────────────────────────────────────────────────────

    //
    // ─── ROTAS ──────────────────────────────────────────────────────────────────────
    //

    // Default
    $app->get('/', 'index');
    // Matrículas
    $app->get('/matriculas', 'get_matriculas');
    $app->get('/matricula/{id}', 'get_matricula_detalhes');
    $app->put('/matricula/{id}', 'editar_matricula');
    $app->post('/matricula', 'cadastrar_matricula');
    $app->delete('/matricula/{id}', 'deletar_matricula');
    // Disciplinas
    $app->get('/disciplinas', 'get_disciplinas');
    $app->get('/disciplina/{id}', 'get_disciplina');
    $app->put('/disciplina/{id}', 'editar_disciplina');
    $app->post('/disciplina', 'cadastrar_disciplina');
    $app->delete('/disciplina/{id}', 'deletar_disciplina');
    // Alunos
    $app->get('/alunos', 'get_alunos');
    $app->get('/aluno/{id}', 'get_aluno_detalhes');
    $app->put('/aluno/{id}', 'editar_aluno');
    $app->post('/aluno', 'cadastrar_aluno');
    $app->delete('/aluno/{id}', 'deletar_aluno');
    // Notas
    $app->get('/nota/{id_disciplina}/{id_aluno}/{id_matricula}', 'get_nota');
    $app->put('/nota/{id}', 'editar_nota');
    
    // ────────────────────────────────────────────────────────────────────────────────

    //
    // ─── FUNÇÕES ────────────────────────────────────────────────────────────────────
    //

    // Default
	function index($request, $response) {
		$t=time();
		$data = (date("d - m - Y",$t));
		$helloMensage = array("mensagem" => "$data ~ Seja bem-vindo à API do Giggle");
        return $response->withJson($helloMensage, 200);
    }

    // Includes
    include 'endpoints/matricula.php';
    include 'endpoints/disciplina.php';
    include 'endpoints/aluno.php';
    include 'endpoints/nota.php';

    // ────────────────────────────────────────────────────────────────────────────────

$app->run();

?>