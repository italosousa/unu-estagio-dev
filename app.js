'use strict';

var app = angular.module('Giggle', [
  'ngRoute',
  'Giggle.home',
  'Giggle.matricula',
  'Giggle.aluno',
  'Giggle.disciplina'
])
.config(['$locationProvider', '$routeProvider', function($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('');

  $routeProvider.otherwise({redirectTo: '/home'});
}]);

app.run(function ($rootScope, $location) {
 
  var history = [];

  $rootScope.$on('$routeChangeSuccess', function() {
      console.log($location.$$path)
      history.push($location.$$path);
  });

  $rootScope.paginaAnterior = function () {
      var prevUrl = history.length > 1 ? history.splice(-2)[0] : "/";
      $location.path(prevUrl);
  };
});