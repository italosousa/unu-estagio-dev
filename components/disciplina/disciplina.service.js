matriculaModule.factory('disciplinaService', function($http) {
    return {
        get_disciplinas: function() {
            return $http.get('/api/disciplinas').then(function(response) {
                return response.data;
            });
        },
        get_disciplina: function(idDisciplina) {
            rota = 'api/disciplina/' + idDisciplina;
            console.log(rota);
            return $http.get(rota).then(function(response) {
                console.log('retornou');
                return response.data;
            });
        },
        editar_disciplina: function(idDisciplina, data) {
            rota = 'api/disciplina/' + idDisciplina;
            return $http.put(rota, data)
            .then(function(response) {
                console.log('response', response);
                return response.data;
            }, function(response) {
                return response.data;
            });
        },
        cadastrar_disciplina: function(data) {
            return $http.post('/api/disciplina', data)
            .then(function(response) {
                    return response.data;
            }, 
            function(response) { 
                    return response.data;
            });
        },
        get_notas: function(idDisciplina, idAluno, idMatricula) {
            rota = 'api/nota/' + idDisciplina + '/' + idAluno + '/' + idMatricula;
            console.log(rota);
            return $http.get(rota).then(function(response) {
                console.log('retornou');
                return response.data;
            });
        },
        editar_nota: function(idNota, data){
            rota = 'api/nota/' + idNota;
            return $http.put(rota, data)
            .then(function(response) {
                console.log('response', response);
                return response.data;
            }, function(response) {
                return response.data;
            });
        },
        excluir_disciplina: function(idDisciplina) {
            rota = '/api/disciplina/' + idDisciplina;
            return $http.delete(rota)
            .then(function(response){
                return response.data;
            });
        }
    }
});
