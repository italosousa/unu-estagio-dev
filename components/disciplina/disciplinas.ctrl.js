disciplinaModule.controller('DisciplinasCtrl', ['$scope', 'disciplinaService', function($scope, disciplinaService) {

    // requisicao
    disciplinaService.get_disciplinas().then(function(data){
        $scope.disciplinas = data;
    });

    // remove Modal
    var toRemove = null;
    $scope.toRemoveObj = null;

    $scope.zerarRemove = function(){
        var toRemove = null;
        var toRemoveObj = null;
    };

    $scope.removeAlertModal = function(id){
        toRemove = id;
        $scope.toRemoveObj = $scope.disciplinas[id];
    };

    $scope.removeDisciplinaModal = function(){
        console.log(toRemove);
        disciplinaService.excluir_disciplina(toRemove).then(function(data){
            console.log(data);
            swal("Sucesso", data.texto, data.status);
            disciplinaService.get_disciplinas().then(function(data){
                $scope.disciplinas = data;
            });
        });
        $scope.zerarRemove();
    };

}]);