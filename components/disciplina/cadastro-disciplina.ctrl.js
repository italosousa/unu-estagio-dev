disciplinaModule.controller('CadastroDisciplinaCtrl', ['$scope', 'disciplinaService',  function($scope, disciplinaService) {

    $scope.cadastro = {
        nome: ''
    };

    $scope.salvarDados = function() {
        disciplinaService.cadastrar_disciplina($scope.cadastro).then(function(data){
            console.log(data)
            swal("Sucesso", data.texto, data.status);
            $scope.paginaAnterior();
        });
    };

}]);