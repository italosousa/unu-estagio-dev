disciplinaModule.controller('EditarNotaCtrl', ['$scope', '$routeParams', 'disciplinaService', function($scope, $routeParams, disciplinaService) {

    $scope.idDisciplina = $routeParams.id_disciplina;
    $scope.idAluno = $routeParams.id_aluno;
    $scope.idMatricula = $routeParams.id_matricula;
    
    disciplinaService.get_notas($scope.idDisciplina, $scope.idAluno, $scope.idMatricula).then(function(data){
        $scope.dadosNota = data;
        console.log($scope.dadosNota);
    });

    $scope.salvarDados = function() {
        disciplinaService.editar_nota($scope.dadosNota.id, $scope.editNota).then(function(data){
            console.log(data)
            swal("Sucesso", data.texto, data.status);
            $scope.paginaAnterior();
        });
    };

}]);