disciplinaModule.controller('DetalheDisciplinaCtrl', ['$scope', '$routeParams','disciplinaService', function($scope, $routeParams, disciplinaService) {

    $scope.idDisciplina = $routeParams.id_disciplina;
    
    disciplinaService.get_disciplina($scope.idDisciplina).then(function(data){
        $scope.disciplina = data;
    });

    $scope.criterioDeOrdenacao = 'media';
    $scope.direcaoDeOrdenacao = !$scope.direcaoDeOrdenacao;
    $scope.ordernarPor = function (campo){
        $scope.criterioDeOrdenacao = campo;	
        $scope.direcaoDeOrdenacao = !$scope.direcaoDeOrdenacao;
    };

}]);
