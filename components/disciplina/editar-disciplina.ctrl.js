disciplinaModule.controller('EditarDisciplinaCtrl', ['$scope', '$routeParams', 'disciplinaService', function($scope, $routeParams, disciplinaService) {

    $scope.idDisciplina = $routeParams.id_disciplina;

    disciplinaService.get_disciplina($scope.idDisciplina).then(function(data){
        $scope.disciplina = data;
    });

    $scope.salvarDados = function() {
        var toSave = {
            id: $scope.disciplina["id"],
            nome: $scope.disciplina["nome"]
        };
        disciplinaService.editar_disciplina($scope.idDisciplina, toSave).then(function(data){
            console.log(data)
            swal("Sucesso", data.texto, data.status);
            $scope.paginaAnterior();
        });
    };

}]);