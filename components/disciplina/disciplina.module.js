'use strict';

var disciplinaModule = angular.module('Giggle.disciplina', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  // rota para disciplinas
  $routeProvider
  .when('/disciplinas', {
    templateUrl: 'components/disciplina/disciplinas.html',
    controller: 'DisciplinasCtrl'
  })
  .when('/cadastro-disciplina', {
    templateUrl: 'components/disciplina/cadastro-disciplina.html',
    controller: 'CadastroDisciplinaCtrl'
  })
  .when('/editar-disciplina/:id_disciplina', {
    templateUrl: 'components/disciplina/editar-disciplina.html',
    controller: 'EditarDisciplinaCtrl'
  })
  .when('/disciplina/:id_disciplina', {
    templateUrl: 'components/disciplina/detalhe-disciplina.html',
    controller: 'DetalheDisciplinaCtrl'
  })
  .when('/disciplina/:id_disciplina/:id_aluno/:id_matricula', {
    templateUrl: 'components/disciplina/editar-nota.html',
    controller: 'EditarNotaCtrl'
  });
}]);