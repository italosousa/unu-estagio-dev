matriculaModule.controller('EditarMatriculaCtrl', ['$scope', '$routeParams', '$timeout', 'matriculaService',  function($scope, $routeParams, $timeout, matriculaService) {
    // parametros de rota
    var id_matricula = $routeParams.id_matricula;
    $scope.idMatricula = id_matricula;
    
    // save
    var convertMatriculas = function(matriculas){
        idMatriculas = [];
        for (matricula of matriculas){
            idMatriculas.push(matricula["id"]);
        }
        return idMatriculas.join(';');
    };

    $scope.salvarDados = function() {
        console.log('entrou');
        var toSave = {
            periodo: $scope.dadosMatricula["periodo"],
            disciplinas: convertMatriculas($scope.dadosMatricula["disciplinas"])
        };
        console.log(toSave);
        matriculaService.editar_matricula($scope.idMatricula, toSave).then(function(data){
            console.log(data)
            swal("Sucesso", data.texto, data.status);
            $scope.paginaAnterior();
        });
    };

    // requisicao
    $scope.periodos = matriculaService.get_periodos();
    matriculaService.get_matricula($scope.idMatricula).then(function(data){
        $scope.dadosMatricula = data[0];
    });

    // dados
    $scope.gradeDisciplinas = [];
    $scope.disciplinasAdicionadas = [];
   
    // edicao
    var removeDisciplina = function(id) {
        $scope.dadosMatricula.disciplinas.splice(id, 1);
    };

    var isMatriculado = function(id) {
       itemId = $scope.gradeDisciplinas[id].id;
       return $scope.dadosMatricula.disciplinas.map(function (item) { return item.id; }).indexOf(itemId);
    };

    // remove Modal
    var toRemove = null;
    $scope.toRemoveObj = null;

    $scope.zerarRemove = function(){
        var toRemove = null;
        var toRemoveObj = null;
    };

    $scope.removeAlertModal = function(id){
        toRemove = id;
        $scope.toRemoveObj = $scope.dadosMatricula.disciplinas[id];
    };

    $scope.removeDisciplinaModal = function(){
        removeDisciplina(toRemove);
        $scope.zerarRemove();
    };
    // fim remove Modal 

    // editar disciplina Modal
    $scope.modalAlert = true;

    $scope.carregarDisciplinas = function() {
        matriculaService.get_disciplinas().then(function(data){
            $scope.gradeDisciplinas = data;
        });
    };

    $scope.disciplinaSelecionada = function(id) {
        retorno = true
        if ($scope.disciplinasAdicionadas.indexOf($scope.gradeDisciplinas[id]) != -1){
            retorno = false;
        } else {
            retorno = true;
        } if (isMatriculado(id) != -1 ) {
            retorno = false;
        }

        return retorno;
    };

    $scope.addDisciplina = function(id) {
        $scope.disciplinasAdicionadas.push($scope.gradeDisciplinas[id]);
    }; 

    $scope.delDisciplina = function(id) {
        idItem = $scope.disciplinasAdicionadas.indexOf($scope.gradeDisciplinas[id]);
        $scope.disciplinasAdicionadas.splice(idItem, 1);
    };
    
    $scope.setAlertDefault = function(){
        $scope.modalAlert = true;
    };

    $scope.closeModal = function() {
        if ($scope.disciplinasAdicionadas.length == 0){
            $scope.modalAlert = false;
        } else {
            for (var disciplina of $scope.disciplinasAdicionadas){
                $scope.dadosMatricula.disciplinas.push(disciplina);
            }
            $scope.disciplinasAdicionadas = [];
            $('#adicionarDisciplinaModal').modal('hide');
        }
    };

}]);