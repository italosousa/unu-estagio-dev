matriculaModule.controller('CadastroMatriculaCtrl', ['$scope', 'matriculaService',  function($scope, matriculaService) {
    // requests
    $scope.periodos = matriculaService.get_periodos();
    matriculaService.get_alunos().then(function(data){
        $scope.alunos = data;
    });
    matriculaService.get_disciplinas().then(function(data){
        $scope.disciplinas = data;
    });

    // save
    var convertMatriculas = function(matriculas){
        idMatriculas = [];
        for (matricula of matriculas){
            idMatriculas.push(matricula["id"]);
        }
        return idMatriculas.join(';');
    };

    $scope.salvarDados = function() {
        console.log('entrou');
        var toSave = {
            id_aluno: $scope.cadastro["aluno"]["id"],
            periodo: $scope.cadastro["periodo"],
            disciplinas: convertMatriculas($scope.cadastro["disciplinas"])
        };
        console.log(toSave);
        matriculaService.cadastrar_matricula(toSave).then(function(data){
            console.log(data)
            swal("Sucesso", data.texto, data.status);
            $scope.paginaAnterior();
        });
    }

    //obj cadastro
    $scope.cadastro = {
        aluno: 'Selecione um Aluno',
        periodo: 'Selecione um Período',
        disciplinas: []
    }
    
    // alerta vazio 
    $scope.modalAlert = true;

    $scope.setAlertDefault = function(){
        $scope.modalAlert = true;
    };

    // adicionar disciplina Modal
    $scope.disciplinasAdicionadas = [];

    var isMatriculado = function(id) {
        itemId = $scope.disciplinas[id].id;
        return $scope.cadastro.disciplinas.map(function (item) { return item.id; }).indexOf(itemId);
    };

    $scope.disciplinaSelecionada = function(id) {
        retorno = true
        if ($scope.disciplinasAdicionadas.indexOf($scope.disciplinas[id]) != -1){
            retorno = false;
        } else {
            retorno = true;
        } 
        if (isMatriculado(id) != -1 ) {
            retorno = false;
        }

        return retorno;
    };

    $scope.zerarModal = function() {
        $scope.disciplinasAdicionadas = [];
    }

    $scope.addDisciplina = function(id) {
        $scope.disciplinasAdicionadas.push($scope.disciplinas[id]);
    }; 

    $scope.delDisciplina = function(id) {
        idItem = $scope.disciplinasAdicionadas.indexOf($scope.disciplinas[id]);
        $scope.disciplinasAdicionadas.splice(idItem, 1);
    };

    $scope.closeModal = function() {
        if ($scope.disciplinasAdicionadas.length == 0){
            $scope.modalAlert = false;
        } else {
            for (var disciplina of $scope.disciplinasAdicionadas){
                $scope.cadastro.disciplinas.push(disciplina);
            }
            $scope.disciplinasAdicionadas = [];
            $('#adicionarDisciplinaModal').modal('hide');
        }
    };

    // remove Modal
    var toRemove = null;
    $scope.toRemoveObj = null;

    var removeDisciplina = function(id) {
        $scope.cadastro.disciplinas.splice(id, 1);
    };

    $scope.zerarRemove = function(){
        $scope.modalAlert = true;
        var toRemove = null;
        var toRemoveObj = null;
    };

    $scope.removeAlertModal = function(id){
        toRemove = id;
        $scope.toRemoveObj = $scope.cadastro.disciplinas[id];
    };

    $scope.removeDisciplinaModal = function(){
        removeDisciplina(toRemove);
        $scope.zerarRemove();
    };
    // fim remove Modal 

}]);