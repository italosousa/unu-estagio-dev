matriculaModule.controller('DetalheMatriculaCtrl', ['$scope', '$routeParams', 'matriculaService',  function($scope, $routeParams, matriculaService) {

    $scope.idMatricula = $routeParams.id_matricula;

    matriculaService.get_matricula($scope.idMatricula).then(function(data){
        $scope.matricula = data[0];
    });

    $scope.criterioDeOrdenacao = 'nome';
    $scope.ordernarPor = function (campo){
        $scope.criterioDeOrdenacao = campo;	
        $scope.direcaoDeOrdenacao = !$scope.direcaoDeOrdenacao;
    };

}]);