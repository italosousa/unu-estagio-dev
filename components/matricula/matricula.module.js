'use strict';

var matriculaModule = angular.module('Giggle.matricula', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider
  // rota para matriculas
  .when('/matriculas', {
    templateUrl: 'components/matricula/matriculas.html',
    controller: 'MatriculasCtrl'
  })
  .when('/cadastro-matricula', {
    templateUrl: 'components/matricula/cadastro-matricula.html',
    controller: 'CadastroMatriculaCtrl'
  })
  .when('/editar-matricula/:id_matricula', {
    templateUrl: 'components/matricula/editar-matricula.html',
    controller: 'EditarMatriculaCtrl'
  })
  .when('/matricula/:id_matricula', {
    templateUrl: 'components/matricula/detalhe-matricula.html',
    controller: 'DetalheMatriculaCtrl'
  });
}]);