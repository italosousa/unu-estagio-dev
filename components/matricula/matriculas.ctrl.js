matriculaModule.controller( 'MatriculasCtrl', ['$scope', 'matriculaService', 'matriculaService', function($scope, matriculaService, matriculaService) {
    // requisicao
    matriculaService.get_matriculas().then(function(data){
        $scope.matriculas = data;
    });

    // remocao - virar requisicao
    

    // mais detalhe Modal
    $scope.detalheMatricula;
    $scope.maisDetalhe = function(id){
        $scope.detalheMatricula = $scope.matriculas[id]; 
    };

    $scope.criterioDeOrdenacao = 'id';
    $scope.ordernarPor = function (campo){
        $scope.criterioDeOrdenacao = campo;	
        $scope.direcaoDeOrdenacao = !$scope.direcaoDeOrdenacao;
    };

    // remove Modal
    var toRemove = null;
    $scope.toRemoveObj = null;

    $scope.zerarRemove = function(){
        var toRemove = null;
        var toRemoveObj = null;
    };

    $scope.removeAlertModal = function(id){
        toRemove = id;
        $scope.toRemoveObj = $scope.matriculas[id];
    };

    $scope.removeMatriculaModal = function(){
        console.log(toRemove);
        matriculaService.excluir_matricula(toRemove).then(function(data){
            console.log(data);
            swal("Sucesso", data.texto, data.status);
            matriculaService.get_matriculas().then(function(data){
                $scope.matriculas = data;
            });
        });
        $scope.zerarRemove();
    };

}])