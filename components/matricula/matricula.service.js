matriculaModule.factory('matriculaService', function($http) {
    return {
        get_periodos: function() {
            return [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        },
        get_matriculas: function() {
            return $http.get('/api/matriculas').then(function(response) {
                return response.data;
            });
        },
        get_matricula: function(idMatricula) {
            rota = 'api/matricula/' + idMatricula;
            console.log(rota);
            return $http.get(rota).then(function(response) {
                console.log('retornou');
                return response.data;
            });
        },
        get_disciplinas: function() {
            return $http.get('/api/disciplinas').then(function(response) {
                return response.data;
            });
        },
        get_alunos: function() {
            return $http.get('/api/alunos').then(function(response) {
                return response.data;
            });
        },
        editar_matricula: function(idMatricula, data) {
            rota = 'api/matricula/' + idMatricula;
            return $http.put(rota, data)
            .then(function(response) {
                console.log('response', response);
                return response.data;
            }, function(response) {
                return response.data;
            });
        },
        cadastrar_matricula: function(data) {
            return $http.post('/api/matricula', data)
            .then(function(response) {
                    return response.data;
            }, 
            function(response) { 
                    return response.data;
            });
        },
        excluir_matricula: function(idMatricula) {
            rota = '/api/matricula/' + idMatricula;
            return $http.delete(rota)
            .then(function(response){
                return response.data;
            });
        }
    }
});