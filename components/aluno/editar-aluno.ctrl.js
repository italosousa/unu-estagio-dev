alunoModule.controller('EditarAlunoCtrl', [ '$scope', '$routeParams', 'alunoService', function($scope, $routeParams, alunoService) {
    // parametros de rota
    var id_aluno = $routeParams.id_aluno;
    $scope.idAluno = id_aluno;

    // $scope.aluno = alunoService.get_aluno(id_aluno);
    alunoService.get_aluno($scope.idAluno).then(function(data){
        $scope.aluno = data;
    });

    var convertMatriculas = function(matriculas){
        idMatriculas = [];
        for (matricula of matriculas){
            idMatriculas.push(matricula["id"]);
        }
        return idMatriculas.join(';');
    };

    $scope.salvarDados = function() {
        console.log('entrou');
        var toSave = {
            id: $scope.aluno["id"],
            nome: $scope.aluno["nome"],
            data: $scope.aluno["data"],
            matriculas: convertMatriculas($scope.aluno["matriculas"])
        };
        console.log(toSave);
        alunoService.editar_aluno($scope.idAluno, toSave).then(function(data){
            console.log(data)
            swal("Sucesso", data.texto, data.status);
            $scope.paginaAnterior();
        });
    };

    // remove Modal
    var toRemove = null;
    $scope.toRemoveObj = null;

    $scope.zerarRemove = function(){
        var toRemove = null;
        var toRemoveObj = null;
    };

    $scope.removeAlertModal = function(id){
        toRemove = id;
        $scope.toRemoveObj = $scope.matriculas[id];
    };

    $scope.removeMatriculaModal = function(){
        removeMatricula(toRemove);
        $scope.zerarRemove();
    };
}]);