alunoModule.controller('AlunosCtrl', [ '$scope', 'alunoService', function($scope, alunoService) {
    alunoService.get_alunos().then(function(data){
        $scope.alunos = data;
    });

    // remocao
    var removeAluno = function(id) {
        $scope.alunos.splice(id, 1);
    };

    // remove Modal
    var toRemove = null;
    $scope.toRemoveObj = null;

    $scope.zerarRemove = function(){
        var toRemove = null;
        var toRemoveObj = null;
    };

    $scope.removeAlertModal = function(id){
        toRemove = id;
        $scope.toRemoveObj = $scope.alunos[id];
    };

    $scope.removeAlunoModal = function(){
        console.log(toRemove);
        alunoService.excluir_aluno(toRemove).then(function(data){
            console.log(data);
            swal("Sucesso", data.texto, data.status);
            alunoService.get_alunos().then(function(data){
                $scope.alunos = data;
            });
        });
        $scope.zerarRemove();
    };
}]);