alunoModule.factory('alunoService', function($http) {
    return {
        get_alunos: function() {
            return $http.get('/api/alunos').then(function(response) {
                return response.data;
            });
        },
        get_aluno: function(idAluno) {
            rota = '/api/aluno/' + idAluno;
            return $http.get(rota).then(function(response) {
                return response.data[0];
            }); 
        },
        editar_aluno: function(idAluno, data) {
            rota = 'api/aluno/' + idAluno;
            return $http.put(rota, data)
            .then(function(response) {
                console.log('response', response);
                return response.data;
            }, function(response) {
                return response.data;
            });
        },
        cadastrar_aluno: function(data) {
            return $http.post('/api/aluno', data)
            .then(function(response) {
                    return response.data;
            }, 
            function(response) { 
                    return response.data;
            });
        },
        excluir_aluno: function(idAluno) {
            rota = '/api/aluno/' + idAluno;
            return $http.delete(rota)
            .then(function(response){
                return response.data;
            });
        }
    }
});