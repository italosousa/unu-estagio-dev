'use strict';

var alunoModule = angular.module('Giggle.aluno', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  // rota para alunos
  $routeProvider
  .when('/alunos', {
    templateUrl: 'components/aluno/alunos.html',
    controller: 'AlunosCtrl'
  })
  .when('/cadastro-aluno', {
    templateUrl: 'components/aluno/cadastro-aluno.html',
    controller: 'CadastroAlunoCtrl'
  })
  .when('/editar-aluno/:id_aluno', {
    templateUrl: 'components/aluno/editar-aluno.html',
    controller: 'EditarAlunoCtrl'
  })
  .when('/aluno/:id_aluno', {
    templateUrl: 'components/aluno/detalhe-aluno.html',
    controller: 'DetalheAlunoCtrl'
  })
}]);

