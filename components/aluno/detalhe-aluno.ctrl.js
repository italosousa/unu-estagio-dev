matriculaModule.controller('DetalheAlunoCtrl', ['$scope', '$routeParams', 'alunoService',  function($scope, $routeParams, alunoService) {

    $scope.idAluno = $routeParams.id_aluno;

    // $scope.aluno = alunoService.get_aluno($scope.idAluno);
    // $scope.disciplinas = alunoService.get_disciplinas($scope.idAluno);
    alunoService.get_aluno($scope.idAluno).then(function(data){
        $scope.aluno = data;
        console.log($scope.aluno);
    });

    $scope.criterioDeOrdenacao = 'nome';
    $scope.ordernarPor = function (campo){
        $scope.criterioDeOrdenacao = campo;	
        $scope.direcaoDeOrdenacao = !$scope.direcaoDeOrdenacao;
    };

}]);