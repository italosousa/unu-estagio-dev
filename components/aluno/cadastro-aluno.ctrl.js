alunoModule.controller('CadastroAlunoCtrl', ['$scope', 'alunoService', function($scope, alunoService) {

    $scope.cadastro = {
        nome: '',
        data: '',
    }

    $scope.salvarDados = function() {
        alunoService.cadastrar_aluno($scope.cadastro).then(function(data){
            console.log(data);
            swal("Sucesso", data.texto, data.status);
            $scope.paginaAnterior();
        });
    }

}]);