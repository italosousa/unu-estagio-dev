# SELEÇÃO DE ESTÁGIO EM DESENVOLVIMENTO WEB #

# PROJETO #

Desenvolva um mini sistema de gerenciamento acadêmico, o sistema deverá conter:

* Cadastro, Alteração, Remoção e Detalhamento de:
  * Matrículas;
  * Alunos;
  * Disciplinas;

Os alunos irão possuir: id, nome, data de nascimento;

As disciplinas irão possuir: id, nome;

As matrículas irão possuir: id, período, aluno e disciplina(s);

O sistema deverá permitir que seja registrado para cada Aluno duas notas em uma determinada disciplina;
 
No detalhamento do aluno serão exibidas:

* Nome;
* Matriculas;
  * Disciplinas;
    * Nome da Disciplina;
    * Notas do aluno na Disciplina;
    * Média do aluno na Disciplina;
* Média GERAL;

No detalhamento da disciplina serão exibidas:

* A listagem de alunos ordenados da maior para menor média.

# INFORMAÇÕES TÉCNICAS #

* O projeto deverá ser desenvolvido utilizando PHP para o backend e Mysql para o banco de dados.

* O repositório deverá conter todos os códigos e artefatos desenvolvidos para o projeto.

* O repositório deverá conter o script de criação das tabelas do banco de dados.

# PONTOS EXTRAS #

* Utilização de FrameWork Javascript ( Preferencialmente AngularJs );

* Bootstrap;

* Arquitetura de Camadas;

* Testes unitários;

# AVALIAÇÃO #

Serão avaliados os seguintes aspectos:

  * Lógica de desenvolvimento;
  * Organização e Estrutura do código;
  * Modelagem do Banco de Dados;

# INSTRUÇÕES #

* Você deverá realizar um fork deste repositório e desenvolver todo o seu projeto no SEU repositório. 

* O repositório deverá ser público para que possamos realizar a correção do teste.

* Os projetos deverão ser enviados via PULL REQUEST até às 23:59 do dia dia 10/02/2017.

# ATENÇÃO #

* Não se deve tentar fazer o PUSH diretamente para ESTE repositório!

